# -*- coding: utf-8 -*-

"""
    for installing with pip
"""

from distutils.core import setup
from setuptools import find_packages

setup(
    name='displayable',
    version='1.2.0',
    author='Mark V',
    author_email='noreply.mail.nl',
    packages=find_packages(),
    include_package_data=True,
    url='git+https://bitbucket.org/mverleg/django_displayable.git',
    license='revised BSD license; see LICENSE.txt',
    description='Django Displayable lets you turn models into displayable elements, including inheritance',
    zip_safe=True,
    install_requires = [],
)
