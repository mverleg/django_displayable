
"""
	Template tags used by displayable subclasses.
"""

from django.template.base import Library
from displayable.classes import ChainDisplayable, DirectDisplayable
from displayable.misc import ArgumentError, require_request


register = Library()


@register.simple_tag(name = 'display', takes_context = True)
@require_request
def display(context, request, displayable, **kwargs):
	"""
		Display a Displayable (to be called from outside the class).
	"""
	if isinstance(displayable, ChainDisplayable):
		displayable.displayable_chain = displayable._chain()[:-1]
		for item in displayable.displayable_chain:
			''' these are all references to the same list '''
			item.displayable_chain = displayable.displayable_chain
		return display_child(context, displayable, **kwargs)
	elif isinstance(displayable, DirectDisplayable):
		return displayable.display(
			context = context,
			request = request,
			**kwargs
		)
	if displayable == '':
		raise ArgumentError('{% display %} tag invoked with empty string argument; this likely means that the ' +
			'variable passed to {% display %} was not found; argument should be a Displayable subclass')
	raise ArgumentError('{%% display %%} tag invoked with an unknown argument type %s; argument should be a ' +
		'Displayable subclass (or maybe you are looking for display_child?)' % type(displayable))


@register.simple_tag(name = 'display_child', takes_context = True)
@require_request
def display_child(context, request, displayable, **kwargs):
	"""
		Dislay the child of a Displayable (e.g. Reactable), to be called
		from Displayable's display template,
	"""
	if not hasattr(displayable, 'displayable_chain'):
		raise Exception('it seems that you are invoking the display_child tag without having invoked display ' +
			'first; normally you would call display for the object you want to display, and that object will ' +
			'call display_child as necessary; (it could also mean you are passing another instance to ' +
			'display_child than to child)')
	try:
		child_displayable = displayable.displayable_chain.pop()
	except IndexError:
		return ''
	try:
		child_html = child_displayable.display(
			context = context,
			request = request,
			chain = displayable.displayable_chain,
			**kwargs
		)
	except RuntimeError as err:
		raise RuntimeError('a RuntimeError occured in the display child tag; it is probably, though not certain, ' +
			'that a Displayable subclass used the display tag rather than display_child. Message: %s' % err)
	if child_html is None:
		return display_child(context, child_displayable, **kwargs)
	return child_html


