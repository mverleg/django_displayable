
from django.http import HttpResponse
from django.shortcuts import render


'''
	show a single in the content block of extend template
	it is assumed that kwargs contains only the primary key of the target
	  object, perhaps captures from the URL as a string
	url(r'^(?P<mycls_pk>[0-9]+)/$', show_displayable, {'cls': MyCls, 'extend': BASE_TEMPLATE}, name = 'show_mycls'),
'''
def show_displayable(request, cls, extend, title = '', **kwargs):
	pk = kwargs[next(iter((kwargs.keys())))]
	try:
		displayable = cls.objects.get(pk = int(pk))
	except cls.DoesNotExist:
		return HttpResponse('There is no %s with key %s' % (cls, pk))
	return render(request, 'single_displayable.html', {
		'displayable': displayable,
		'extend': extend,
		'title': title,
	})


'''
	show a list of displayables in the content block of extend template
'''
def list_displayables(request, displayables, extend, title = ''):
	return render(request, 'list_displayables.html', {
		'displayables': displayables,
		'extend': extend,
		'title': title,
	})


