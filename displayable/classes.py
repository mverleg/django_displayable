
"""
	Dhe Displayable class is a class used as parent in order to make
	subclasses displayable with the {% display %} tag.
	(Non-diamond) inheritance works, simply make the display command
	pass on context and render a template that contains a
	{% display_child %} tag somewhere.
"""

from .inheritance import parent_of_type, NoParentException


class DirectDisplayable(object):
	"""
		each Displayable subclass should override this function with the
		appropriate code to display itself (rendered as a string)
	"""

	def display(self, context, request, **kwargs):
		"""
			Displayable itself doesn't display; child classes should
			implement something like this:

			return render_to_string('displayable_root.html', {
				'displayable': self,
			}, context)
		"""
		raise Exception('DirectDisplayable.display() called directly; make sure your subclass %s overrides the .display method' % type(self))


class ChainDisplayable(object):

	def _chain(self):
		"""
			Get an ancestor chain of parents from the calling instance
			to Displayable; this is used for displaying all parents in
			the case of inheritance, and should not be overridden.
			Does not work with diamond inheritance if multiple if
			multiple base classes derive from Displayable.
		"""
		chain, cls = [self], self.__class__
		while cls:
			try:
				cls = parent_of_type(cls, self, Displayable)
				chain.append(cls)
			except NoParentException:
				break
		return chain

	def display(self, context, request, **kwargs):
		"""
			As DirectDisplayable.display, but uses inheritance:
			the template should have a {% display_child %} tag if the class has
			(or might in the future have) any subclasses to be displayed.
			If you don't want to display a class, let .display() return None.
		"""
		raise Exception('Displayable.display() called directly; this probably means that %s or one of it\'s ' + \
			'parent classes did not override .display()' % type(self))


Displayable = ChainDisplayable


