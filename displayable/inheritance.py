
"""
    parent is comparable to super, but it always returns an instance of the first parent
    more info: http://stackoverflow.com/a/22207673/723090
    class C(A):
        def name(self):
            return parent(C, self).name() + 'C'
    print C()
    parent_of_type is the same except that it only considers parents derived from required_type
"""

class NoParentException(Exception):
    pass


class parent(object):
    def __init__(self, cls, obj):
        if len(cls.__bases__):
            self._delegate_cls = cls.__bases__[0]
        else:
            raise NoParentException('parent called for class "%s", which has no base classes' % cls)
        self._delegate_obj = obj
    def __getattr__(self, name):
        x = getattr(self._delegate_cls, name)
        if hasattr(x, '__get__'):
            return x.__get__(self._delegate_obj)
        return x
    def __repr__(self):
        #return 'parent %s of %s' % (self._delegate_cls.__name__, self._delegate_cls.__repr__(self))
        return 'parent %s' % (self._delegate_cls.__name__)

class parent_of_type(parent):
    def __init__(self, cls, obj, required_type):
        type_bases = [base for base in cls.__bases__ if issubclass(base, required_type)]
        if len(type_bases):
            self._delegate_cls = type_bases[0]
        else:
            raise NoParentException('parent called for class "%s", which has no base classes of type "%s"' % (cls, required_type))
        self._delegate_obj = obj


