

class NoRequestException(Exception):
	pass


class ArgumentError(Exception):
	pass


''' decorator to check for and pass request '''
def require_request(func):
	def with_request(context, *args, **kwargs):
		if 'request' in context:
			return func(context, context['request'], *args, **kwargs)
		raise NoRequestException('request not found in context; make sure  that "django.core.context_processors.request" is in settings.TEMPLATE_CONTEXT_PROCESSORS and that you are using RequestContext when rendering')
	return with_request
